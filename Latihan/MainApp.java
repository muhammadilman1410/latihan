
public class MainApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Person person1 = new Person("Eko", "Tegal");
		person1.sayHello("Padepokan 79");
		System.out.println(person1.sayAddress());
		
		Person person2 = new Person();
		person2.name = "Joko";
		person2.address = "Surabaya";
		person2.sayHello("Padepokan 79");
		System.out.println(person2.sayAddress());
		
		Person person3 = new Person();
		person3.name = "Budi";
		person3.address = "Bandung";
		person3.sayHello("Padepokan 79");
		System.out.println(person3.sayAddress());
	}

}
